package com.example.multiviewrecyclerview;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements MyItemClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        List<ItemModel> lists = new ArrayList<>();
        lists.add(new ItemModel("Title One",ItemModel.ONE_TYPE));
        lists.add(new ItemModel("Title two",ItemModel.TWO_TYPE));
        lists.add(new ItemModel("Title One",ItemModel.ONE_TYPE));
        lists.add(new ItemModel("Title two",ItemModel.TWO_TYPE));
        lists.add(new ItemModel("Title One",ItemModel.ONE_TYPE));
        lists.add(new ItemModel("Title two",ItemModel.TWO_TYPE));
        lists.add(new ItemModel("Title One",ItemModel.ONE_TYPE));
        lists.add(new ItemModel("Title two",ItemModel.TWO_TYPE));
        lists.add(new ItemModel("Title One",ItemModel.ONE_TYPE));
        lists.add(new ItemModel("Title two",ItemModel.TWO_TYPE));
        lists.add(new ItemModel("Title One",ItemModel.ONE_TYPE));
        lists.add(new ItemModel("Title two",ItemModel.TWO_TYPE));

        RecyclerView recyclerView = findViewById(R.id.rvResult);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        ItemsAdapter adapter = new ItemsAdapter(lists,this);
        recyclerView.setAdapter(adapter);
        adapter.setOnItemCLickListenerMe(this);
    }

    @Override
    public void myItemClickListener(int p) {
        Toast.makeText(this,"View One :"+p,Toast.LENGTH_LONG).show();
    }
}
